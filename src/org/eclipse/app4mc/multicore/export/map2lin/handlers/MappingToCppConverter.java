package org.eclipse.app4mc.multicore.export.map2lin.handlers;

import java.awt.Window.Type;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.app4mc.multicore.sharelibs.UniversalHandler;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;

import javafx.scene.chart.PieChart.Data;

public class MappingToCppConverter {
	static Amalthea model;
	HashMap<Scheduler, ProcessingUnit> mScheduler2Core = new HashMap<Scheduler, ProcessingUnit>();
	HashMap<Task, Scheduler> mTask2Scheduler = new HashMap<Task, Scheduler>();
	HashMap<Label, Task> mLabel2Task = new HashMap<Label, Task>();
	EList<Label> globalVariables = new BasicEList<>();

	StringBuilder sFileContent = new StringBuilder();

	public void setModel(Amalthea model) {
		this.model = model;
	}
	
	public MappingToCppConverter() {
		
	}

	public void convertModel() {
			
		// Preparation
		this.prepareMaps();

		// Output
		this.includeLibs();
		this.getGlobalVariables();

		this.declareVariables();
		this.declareMutexes();

		this.addMainInit();
		this.initLibraries();
		this.initSharedData();
		this.initMutexes();
		this.initThreads();
		
		this.declareTasks();
		this.createThreads();
		this.allocateThreads();
		
		this.addMainLoop();

		this.addMainFinalize();
		this.createTasks();
		this.createRunnables();
	
	}
	
	private void createRunnables() {
		EList<Runnable> runnables = model.getSwModel().getRunnables();
		
		for (Runnable runnable: runnables) {
			this.appendln("void " + runnable.getName() + "(){");
			this.appendln("");
			this.appendln("//This is dummy text to test Amalthea");
			this.appendln("");
			this.appendln("}");
			this.appendln("");
		}
	}
	
	private void createTasks() {
		EList<Task> tasks = model.getSwModel().getTasks();
		this.appendln("//Creating the tasks");
		for(Task task: tasks) {
			this.appendln("void " + task.getName() + "(){");
			
			List<Runnable> runnables = SoftwareUtil.getRunnableList(task, null);
			this.appendln("");
			this.appendlnt("//declaring the runnables");
		
			for (Runnable runnable: runnables) {
				this.appendlnt(runnable.getName() + "();");
			}
			
			this.appendln("}");
			this.appendln("");
		}		
	}
	
	private void declareTasks() {
		EList<Task> tasks = model.getSwModel().getTasks();
		this.appendlnt("//Declaring the tasks");
		for (Task task: tasks) {
			this.appendlnt(task.getName() + "();");	
		}
		this.appendln("");
	}
	
	private void appendlntt(String s) {
		this.appendln("\t\t" + s);
	}

	private void appendlnt(String s) {
		this.appendln("\t" + s);
	}

	private void appendln(String s) {
		sFileContent.append(s + "\n");
	}

	private void allocateThreads() {
		this.appendlnt("// Core pinning/mapping");
		for (Entry<Task, Scheduler> e : mTask2Scheduler.entrySet()) {
			Task t = e.getKey();
			Scheduler s = e.getValue();
			ProcessingUnit c = mScheduler2Core.get(s);
			String sTaskName = t.getName();
			String sCoreId = c.getName();
			this.appendlnt("placeAThreadToCore(" + sTaskName + ", " + sCoreId + ");");
		}
		this.appendln("");
	}

	private void createThreads() {
		this.appendlnt("// Thread creation");
		//So this is like a declaration of each task that we are iterating
		
		for (Task t : mTask2Scheduler.keySet()) {
			String sTaskName = t.getName();
			this.appendlnt("if(pthread_create(&" + sTaskName + "_thread, NULL, " + sTaskName + ", NULL)) {");
			this.appendlntt("fprintf(stderr, \"Error creating thread\\n\");");
			this.appendlntt("return 1;");
			this.appendlnt("}");
			this.appendln("");
		}
		this.appendln("");
	}

	private void initThreads() {
		this.appendlnt("// Thread objects");
		for (Task t : mTask2Scheduler.keySet()) {
			String sTaskName = t.getName();
			this.appendlnt("pthread_t " + sTaskName + "_thread;");
		}
		this.appendln("");
	}

	private void initMutexes() {
		this.appendlnt("// Initialize mutexes");
		for (Label label: globalVariables) {
			this.appendlnt("pthread_mutex_init(&" + label.getName() + ", NULL);");
		}
		
//		this.appendlnt("pthread_mutex_init(&temperature_lock, NULL);");

		this.appendln("");		
	}

	private void initSharedData() {
		
		for (Label label: globalVariables) {
			this.appendlnt(label.getName() + " = 0;");
		}

		this.appendln("");
	}

	private void initLibraries() {
		this.appendlnt("// Initialize Libraries");
		this.appendlnt("wiringPiSetup();");
		this.appendln("");
	}

	private void declareMutexes() {
		this.appendln("// Declare mutexes");
		
		for (Label label: globalVariables) {
			this.appendln("pthread_mutex_t " + label.getName() + "_lock;");
		}

		this.appendln("");
	}

	private void declareVariables() {		
		this.appendln("// Declare shared data");
		
		for (Label label: globalVariables) {
			this.appendln("uint" + label.getSize().getValue().intValue() + "_t " + label.getName() + ";");
		}
		
		this.appendln("");
	}

	private void addMainInit() {
		this.appendln("// Enter main() method");
		this.appendln("int main()");
		this.appendln("{");
		this.appendln("");
	}

	private void addMainLoop() {
		this.appendlnt("// Main Loop");
		this.appendlnt("while(1)");
		this.appendlnt("{");
		this.appendlntt("// ...");
		this.appendlnt("}");
		this.appendln("");
	}

	private void addMainFinalize() {
		this.appendlnt("// Exit main() method");
		this.appendlnt("pthread_exit(NULL);");
		this.appendlnt("return 0;");
		this.appendln("}");
		this.appendln("");
	}

	private void includeLibs() {
		this.appendln("#include \"RaspberryTest.h\"");
		this.appendln("");
		this.appendln("#include \"pthread_distribution_lib/pthread_distribution.h\"");
		this.appendln("");
		this.appendln("#include \"tasks/ultrasonic_sensor_task.h\"");
		this.appendln("#include \"tasks/rover_test_task.h\"");
		this.appendln("#include \"tasks/temperature_task.h\"");
		this.appendln("#include \"tasks/keycommand_task.h\"");
		this.appendln("#include \"tasks/motordriver_task.h\"");
		this.appendln("#include \"tasks/infrared_distance_task.h\"");
		this.appendln("#include \"tasks/display_sensors_task.h\"");
		this.appendln("");
		this.appendln("#include \"interfaces.h\"");
		this.appendln("");
	}

	private void getGlobalVariables() {
		EList<Label> labels = model.getSwModel().getLabels();
		EList<Task> tasks = model.getSwModel().getTasks();
		
		System.out.println("********************************");
		
		for (Label label : labels) {
			int cnt = 0;
			
			for (Task task : tasks) {				
				if (SoftwareUtil.getReadLabelSet(task, null).contains(label) || SoftwareUtil.getWriteLabelSet(task, null).contains(label)) {
					cnt++;
				}
			}

			if (cnt > 1) {
				System.out.println("Label " + label.getName() + " is a global variable");
				globalVariables.add(label);
			}
		}
		
		System.out.println("********************************");		
	}

	private void prepareMaps() {
		EList<SchedulerAllocation> schedulerAllocation = this.model.getMappingModel().getSchedulerAllocation();
		// Iterate all Cores / Schedulers
		if (schedulerAllocation.size() <= 0) {
			// Error!
		}
		for (SchedulerAllocation ca : schedulerAllocation) {
			// Check if executingPU is set! If not, fall back to responsibility-List, and
			// allocate scheduler to first core in list.
			if (null != ca.getExecutingPU()) {
				mScheduler2Core.put(ca.getScheduler(), ca.getExecutingPU());
			} else {
				mScheduler2Core.put(ca.getScheduler(), ca.getResponsibility().get(0));
			}

		}

		EList<TaskAllocation> taskAllocation = this.model.getMappingModel().getTaskAllocation();
		if (taskAllocation.size() <= 0) {
			// Error!
		}
		// Iterate all Task Allocation
		for (TaskAllocation ta : taskAllocation) {
			mTask2Scheduler.put(ta.getTask(), ta.getScheduler());
		}
	}

	public StringBuilder getFileContent() {
		return this.sFileContent;
	}
}
