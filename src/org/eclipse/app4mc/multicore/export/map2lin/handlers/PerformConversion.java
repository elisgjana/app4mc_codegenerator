package org.eclipse.app4mc.multicore.export.map2lin.handlers;

import java.io.File;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.multicore.export.map2lin.Map2lin;
import org.eclipse.app4mc.multicore.sharelibs.UniversalHandler;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;



/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class PerformConversion extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public PerformConversion() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		/*
		 * Initialize Plugin
		 */
		UniversalHandler.getInstance().setPluginId(Map2lin.getPluginId());
		UniversalHandler.getInstance().setLog(Map2lin.getDefault().getLog());
		/*
		 * Phase 1: Read the model
		 * Get selected file and determine the full path
		 */
		@SuppressWarnings("deprecation")
		final IFile modelFile = UniversalHandler.getSelectedFile(event);
		final String fileName = new File(modelFile.getFullPath().toOSString()).getName();
		final String rawFileName = fileName.substring(0, fileName.lastIndexOf('.'));
		/*
		 * Drop the cache in the model handler and read the whole model content
		 */
		UniversalHandler.getInstance().dropCache();
		UniversalHandler.getInstance()
				.readModels(URI.createPlatformResourceURI(modelFile.getFullPath().toOSString(), true), true);
		Amalthea model = UniversalHandler.getInstance().getCentralModel();

		/*
		 * Phase 2: Logical operations with the model
		 * Initialize the class for performing model operations
		 */
		final MappingToCppConverter converter = new MappingToCppConverter();

		/*
		 * Set the input and process the model
		 */
		converter.setModel(model);
		converter.convertModel();

		/*
		 * Phase 3: Output
		 * Generate the output path for the resulting file
		 */
		IPath projectPath = modelFile.getProject().getFullPath();
		projectPath = projectPath.append("/" + rawFileName + ".c");

		/*
		 * DEBUG: Print the output and the path to the resulting file
		 */
		System.out.println("CON:\n" + converter.getFileContent().toString());
		System.out.println("URI:\n" + projectPath);
		UniversalHandler.getInstance().writeFile(projectPath, converter.getFileContent());

		return null;
	}
}
